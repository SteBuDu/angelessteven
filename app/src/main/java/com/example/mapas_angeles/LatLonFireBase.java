package com.example.mapas_angeles;

public class LatLonFireBase {
    private double Latitud;
    private double Longitud;
    public LatLonFireBase(){

    }

    public double getLatitud() {
        return Latitud;
    }

    public void setLatitud(double latitud) {
        this.Latitud = latitud;
    }

    public double getLongitud() {
        return Longitud;
    }

    public void setLongitud(double longitud) {
        this.Longitud = longitud;
    }
}
